# Mohamed Karkotly

Personal portfolio that demonstrates my profile and general information.

## Website

[Mohamed Karkotly](https://mohamed-karkotly.web.app)

## Sections
1) **Home**
2) **About**
3) **Servies**
4) **Experience**
5) **Works**
6) **Contact**

## Supported Languages
• **English**

• **Arabic**

## License
[MIT](https://choosealicense.com/licenses/mit/)
