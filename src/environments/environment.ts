// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBOUoloR7Vq-WorxqnDSfb0nxKDn-k-TqU',
    authDomain: 'mohamed-karkotly.firebaseapp.com',
    projectId: 'mohamed-karkotly',
    storageBucket: 'mohamed-karkotly.appspot.com',
    messagingSenderId: '494223516663',
    appId: '1:494223516663:web:5da23af94cc6ae741404f2',
    measurementId: 'G-3XPZRK61YS',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
