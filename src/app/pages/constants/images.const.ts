import { Image } from '@pages/models/image.interface';

export const images: Image[] = [
  {
    name: 'Angular',
    src: 'assets/images/angular.svg',
    alt: 'Angular Logo',
  },
  {
    name: 'Ionic',
    src: 'assets/images/ionic.svg',
    alt: 'Ionic Logo',
  },
  {
    name: 'Electron',
    src: 'assets/images/electron.svg',
    alt: 'Electron Logo',
  },
  {
    name: 'TypeScript',
    src: 'assets/images/typescript.svg',
    alt: 'TypeScript Logo',
  },
];
