import { Box } from '@pages/models/box.interface';

export const boxes: Box[] = [
  {
    countUp: 150000,
    caption: 'about.box1',
  },
  {
    countUp: 6000,
    caption: 'about.box2',
  },
  {
    countUp: 1300,
    caption: 'about.box3',
  },
  {
    countUp: 10000,
    caption: 'about.box4',
  },
];
