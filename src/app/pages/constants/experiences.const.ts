import { Experience } from '@pages/models/experience.interface';

export const experiences: Experience[] = [
  {
    icon: 'fas fa-city',
    title: 'experience.title6',
    author: 'experience.author6',
    date: 'experience.date6',
    description: 'experience.description6',
  },
  {
    icon: 'far fa-building',
    title: 'experience.title5',
    author: 'experience.author5',
    date: 'experience.date5',
    description: 'experience.description5',
  },
  {
    icon: 'fas fa-terminal',
    title: 'experience.title1',
    author: 'experience.author1',
    date: 'experience.date1',
    description: 'experience.description1',
  },
  {
    icon: 'fas fa-building',
    title: 'experience.title2',
    author: 'experience.author2',
    date: 'experience.date2',
    description: 'experience.description2',
  },
  {
    icon: 'fas fa-university',
    title: 'experience.title3',
    author: 'experience.author3',
    date: 'experience.date3',
    description: 'experience.description3',
  },
  {
    icon: 'fas fa-user-graduate',
    title: 'experience.title4',
    author: 'experience.author4',
    date: 'experience.date4',
    description: 'experience.description4',
  },
];
