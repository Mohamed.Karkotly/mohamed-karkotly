import { Service } from '@pages/models/service.interface';

export const services: Service[] = [
  {
    icon: 'diamond-outline',
    title: 'services.title1',
    description: 'services.description1',
  },
  {
    icon: 'color-wand-outline',
    title: 'services.title2',
    description: 'services.description2',
  },
  {
    icon: 'speedometer-outline',
    title: 'services.title3',
    description: 'services.description3',
  },
  {
    icon: 'heart-circle-outline',
    title: 'services.title4',
    description: 'services.description4',
  },
  {
    icon: 'layers-outline',
    title: 'services.title5',
    description: 'services.description5',
  },
  {
    icon: 'settings-outline',
    title: 'services.title6',
    description: 'services.description6',
  },
];
