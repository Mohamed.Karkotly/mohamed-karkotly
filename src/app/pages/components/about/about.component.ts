import { Component } from '@angular/core';
import { boxes } from '@pages/constants/boxes.const';
import { Box } from '@pages/models/box.interface';
import { CountUpOptions } from '@pages/models/count-up.interface';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent {
  boxes: Box[];
  options: CountUpOptions = {};

  constructor() {
    this.boxes = boxes;
    this.options.duration = 3;
  }
}
