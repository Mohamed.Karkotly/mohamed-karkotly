import { Component, OnInit } from '@angular/core';
import { images } from '@pages/constants/images.const';
import { Image } from '@pages/models/image.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  images: Image[];
  constructor() {
    this.images = images;
  }

  ngOnInit() {}
}
