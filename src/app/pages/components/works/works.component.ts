import { Component, OnInit } from '@angular/core';
import { works } from '@pages/constants/works.const';
import { Work } from '@pages/models/work.interface';

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.scss'],
})
export class WorksComponent implements OnInit {
  works: Work[];
  constructor() {
    this.works = works;
  }

  ngOnInit() {}
}
