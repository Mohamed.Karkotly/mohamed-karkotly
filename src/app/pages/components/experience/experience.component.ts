import { Component, OnInit } from '@angular/core';
import { experiences } from '@pages/constants/experiences.const';
import { Experience } from '@pages/models/experience.interface';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss'],
})
export class ExperienceComponent implements OnInit {
  experiences: Experience[];
  constructor() {
    this.experiences = experiences;
  }

  ngOnInit() {}
}
