import { Component, OnInit } from '@angular/core';
import { services } from '@pages/constants/services.const';
import { Service } from '@pages/models/service.interface';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
})
export class ServicesComponent implements OnInit {
  services: Service[];

  constructor() {
    this.services = services;
  }

  ngOnInit() {}
}
