export interface Work {
  src: string;
  title: string;
  description: string;
  isPublished?: boolean;
  link?: string;
  date: string;
}
