export interface Experience {
  icon: string;
  date: string;
  title: string;
  author: string;
  description: string;
}
