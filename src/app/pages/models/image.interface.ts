export interface Image {
  name?: string;
  src: string;
  alt?: string;
}
