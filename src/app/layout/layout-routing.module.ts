import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from '@pages/components/home/home.component';
import { AboutComponent } from '@pages/components/about/about.component';
import { ServicesComponent } from '@pages/components/services/services.component';
import { ExperienceComponent } from '@pages/components/experience/experience.component';
import { WorksComponent } from '@pages/components/works/works.component';
import { ContactComponent } from '@pages/components/contact/contact.component';
import { ErrorPageComponent } from '@shared/components/error-page/error-page.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        redirectTo: '/',
      },
      {
        path: '',
        component: HomeComponent,
        data: {
          title: 'MK - Home',
        },
      },
      {
        path: 'about',
        component: AboutComponent,
        data: {
          title: 'MK - About',
        },
      },
      {
        path: 'services',
        component: ServicesComponent,
        data: {
          title: 'MK - Services',
        },
      },
      {
        path: 'experience',
        component: ExperienceComponent,
        data: {
          title: 'MK - Experience',
        },
      },
      {
        path: 'works',
        component: WorksComponent,
        data: {
          title: 'MK - Works',
        },
      },
      {
        path: 'contact',
        component: ContactComponent,
        data: {
          title: 'MK - Contact',
        },
      },
      {
        path: '**',
        component: ErrorPageComponent,
        data: {
          title: 'Lost in 404 galaxy',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
