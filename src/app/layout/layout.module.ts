import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SharedModule } from '@shared/shared.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [LayoutComponent],
  imports: [CommonModule, IonicModule, LayoutRoutingModule, SharedModule],
})
export class LayoutModule {}
