export interface Link {
  value: string;
  routerLink: string;
  labelKey: string;
  icon?: string;
}
