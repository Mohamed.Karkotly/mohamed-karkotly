import { Component } from '@angular/core';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss'],
})
export class SocialMediaComponent {
  facebook: string;
  linkedin: string;
  github: string;
  constructor() {
    this.facebook = 'mo.karkotly';
    this.linkedin = 'mohamed-karkotly';
    this.github = 'Mohamed-Karkotly';
  }
}
