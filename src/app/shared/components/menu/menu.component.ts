import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { TranslationService } from '@core/services/translation.service';
import { links } from '@shared/constants/links.const';
import { Link } from '@shared/models/link.interface';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent {
  links: Link[];
  constructor(
    private translationService: TranslationService,
    public menuCtrl: MenuController
  ) {
    this.links = links;
  }

  selectLanguage(event: any) {
    this.translationService.selectLanguage(event);
  }
}
