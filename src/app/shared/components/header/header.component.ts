import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslationService } from '@core/services/translation.service';
import { links } from '@shared/constants/links.const';
import { Link } from '@shared/models/link.interface';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  segment: string;
  tempSegment: string;
  links: Link[];
  constructor(
    private translationService: TranslationService,
    protected router: Router
  ) {
    this.links = links;
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.segment = val.url;
        this.tempSegment = this.segment;
      }
    });
  }

  selectLanguage(event: any) {
    this.segment = this.tempSegment;
    this.translationService.selectLanguage(event);
  }

  navigate(event: any) {
    const route = event.detail.value;
    const valid = links.some((link) => link.value === route);
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    valid && this.router.navigateByUrl(event.detail.value);
  }
}
