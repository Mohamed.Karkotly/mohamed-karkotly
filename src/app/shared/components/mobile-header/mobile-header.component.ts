import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslationService } from '@core/services/translation.service';

@Component({
  selector: 'app-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.scss'],
})
export class MobileHeaderComponent {
  isVisible: boolean;
  isRTL: boolean;
  constructor(
    private translationService: TranslationService,
    public router: Router,
    private location: Location
  ) {
    this.subscribeRouterEvents();
    this.subscribeLayoutDirection();
  }

  subscribeRouterEvents(): void {
    this.router.events.subscribe((val) => {
      this.isVisible =
        val instanceof NavigationEnd && this.router.url.match('[^/]+$')
          ? true
          : false;
    });
  }

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.isRTL = dir === 'rtl' ? true : false;
    });
  }

  goBack() {
    /* this.location.back(); */
    this.router.navigateByUrl('/home');
  }
}
