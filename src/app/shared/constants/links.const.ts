import { Link } from '@shared/models/link.interface';

export const links: Link[] = [
  {
    value: '/',
    routerLink: '',
    labelKey: 'toolbar.home',
    icon: 'home-outline',
  },
  {
    value: '/about',
    routerLink: '/about',
    labelKey: 'toolbar.about',
    icon: 'bulb-outline',
  },
  {
    value: '/services',
    routerLink: '/services',
    labelKey: 'toolbar.services',
    icon: 'settings-outline',
  },
  {
    value: '/experience',
    routerLink: '/experience',
    labelKey: 'toolbar.experience',
    icon: 'school-outline',
  },
  {
    value: '/works',
    routerLink: '/works',
    labelKey: 'toolbar.works',
    icon: 'code-working-outline',
  },
  {
    value: '/contact',
    routerLink: '/contact',
    labelKey: 'toolbar.contact',
    icon: 'mail-outline',
  },
];
