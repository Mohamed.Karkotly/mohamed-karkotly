import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

/* declarations components */
import { HeaderComponent } from './components/header/header.component';
import { MobileHeaderComponent } from './components/mobile-header/mobile-header.component';
import { MenuComponent } from './components/menu/menu.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { SocialMediaComponent } from './components/social-media/social-media.component';
import { ScrollbarThemeDirective } from './directives/scrollbar-theme.directive';

const shared = [
  HeaderComponent,
  MobileHeaderComponent,
  MenuComponent,
  ErrorPageComponent,
  SocialMediaComponent,
  ScrollbarThemeDirective,
];

@NgModule({
  declarations: [...shared],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    FormsModule,
    TranslateModule,
  ],
  exports: [...shared],
})
export class SharedModule {}
