import { Component } from '@angular/core';
import { TitleService } from '@core/services/title.service';
import { TranslationService } from '@core/services/translation.service';
import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  direction: string;
  constructor(
    private titleService: TitleService,
    private translationService: TranslationService
  ) {
    this.translationService.initLanguages();
    this.titleService.changeTitle();
    this.subscribeLayoutDirection();
    const app = initializeApp(environment.firebaseConfig);
    const analytics = getAnalytics(app);
  }

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.direction = dir;
    });
  }
}
