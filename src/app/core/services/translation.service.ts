import { Injectable } from '@angular/core';
import { Language } from '@core/models/language.interface';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  direction: BehaviorSubject<string> = new BehaviorSubject<string>('ltr');
  chosenLanguage: Language;
  constructor(private translateService: TranslateService) {
    this.chosenLanguage = {
      language: localStorage.getItem('lang') || 'en',
      dir: localStorage.getItem('dir') || 'ltr',
    };
  }

  initLanguages() {
    this.translateService.addLangs(['en', 'ar']);
    this.translateService.setDefaultLang(this.chosenLanguage.language);
    this.translateService.use(this.chosenLanguage.language);
    localStorage.setItem('dir', this.chosenLanguage.dir);
    this.direction.next(this.chosenLanguage.dir);
  }

  selectEnglish() {
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
    this.direction.next('ltr');
    localStorage.setItem('lang', 'en');
    localStorage.setItem('dir', 'ltr');
  }

  selectArabic() {
    this.translateService.setDefaultLang('ar');
    this.translateService.use('ar');
    this.direction.next('rtl');
    localStorage.setItem('lang', 'ar');
    localStorage.setItem('dir', 'rtl');
  }

  selectLanguage(event: any) {
    switch (event.detail.value) {
      case 'ar':
        this.selectArabic();
        break;
      case 'en':
        this.selectEnglish();
        break;
      default:
        return;
    }
  }
}
