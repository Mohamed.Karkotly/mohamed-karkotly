import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { httpLoaderFactory } from './utils/http-loader-factory';
import { PagesModule } from '@pages/pages.module';

@NgModule({
  declarations: [],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    /*
    ! WARNING:
    ! The following module import here is meaningless,
    ! but the translation pipe will collapse inside this module if you delete it.
     */
    PagesModule,
  ],
})
export class CoreModule {}
