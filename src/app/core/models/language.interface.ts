export interface Language {
  language: string;
  dir: string;
}
